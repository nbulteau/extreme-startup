Welcome
=======
This is the java version of Robert Chatley and Matt Wynne "Extreme Startup" Ruby version. 

Keywords : Java 8, Spring Boot, thymeleaf, Jetty

- "server" contains the "Extreme Startup" Java version
- "java_jetty_solution" contains the Java solution 
- "log_viewer" is a log viewer in JavaFX XYChart 

I'd like to thank Matt and Robert for the awesome idea and for providing the Ruby version game server!
=======
Finally, I'd like to thank Matt and Robert for the awesome idea and for providing the Ruby version game server! And Stéphane Moreau for the FeetToMetersQuestion, PiQuestion and HexadecimalQuestion (https://github.com/xebia-france/xke-extreme-startup)