package fr.nbu.extremestartup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class ExtremeStartupJavaSolution {

	public static void main(String[] args) {
		System.out.println("eXtreme Startup Server started.");

		SpringApplication.run(ExtremeStartupJavaSolution.class, args);
	}
}
