package fr.nbu.extremestartup.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExtremeStartupController {

	private static final Logger extremeStartupLogger = LoggerFactory.getLogger("ExtremeStartupController");

	// will contain true or false values for the first 10,000 integers
	private static final boolean[] PRIMES = new boolean[10000];

	// setup PRIMES
	static {
		Arrays.fill(PRIMES, true); // assume all integers are prime.
		PRIMES[0] = PRIMES[1] = false; // we know 0 and 1 are not prime.
		for (int i = 2; i < PRIMES.length; i++) {
			// if the number is prime,
			// then go through all its multiples and make their values false.
			if (PRIMES[i]) {
				for (int j = 2; i * j < PRIMES.length; j++) {
					PRIMES[i * j] = false;
				}
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String handler(@RequestParam("q") String q) {
		String query = null;

		if (q != null) {
			try {
				query = URLDecoder.decode(q, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				extremeStartupLogger.info(e.getMessage());
			}
		} else {
			return "";
		}

		// AdditionQuestion
		Matcher additionMatcher = Pattern.compile("what is (\\d+) plus (\\d+)").matcher(query);
		if (additionMatcher.matches()) {
			return String.valueOf(Integer.valueOf(additionMatcher.group(1)) + Integer.valueOf(additionMatcher.group(2)));
		}

		// AdditionAdditionQuestion
		Matcher additionAdditionMatcher = Pattern.compile("what is (\\d+) plus (\\d+) plus (\\d+)").matcher(query);
		if (additionAdditionMatcher.matches()) {
			return String.valueOf(Integer.valueOf(additionAdditionMatcher.group(1)) + Integer.valueOf(additionAdditionMatcher.group(2))
					+ Integer.valueOf(additionAdditionMatcher.group(3)));
		}

		// AdditionMultiplicationQuestion
		Matcher additionMultiplicationMatcher = Pattern.compile("what is (\\d+) plus (\\d+) multiplied by (\\d+)").matcher(query);
		if (additionMultiplicationMatcher.matches()) {
			return String.valueOf(Integer.valueOf(additionMultiplicationMatcher.group(1))
					+ (Integer.valueOf(additionMultiplicationMatcher.group(2)) * Integer.valueOf(additionMultiplicationMatcher.group(3))));
		}

		// AlphagramQuestion
		Matcher alphagramMatcher = Pattern.compile("what is the Alphagram of \"([\\w_]+)\"").matcher(query);
		if (alphagramMatcher.matches()) {
			String word = alphagramMatcher.group(1);
			char[] letters = word.toCharArray();
			Arrays.sort(letters);
			return new String(letters);
		}

		// BlackjackQuestion
		Matcher blackjackMatcher = Pattern.compile(
				"return whichever value is nearest to 21 without going over : (\\d+), (\\d+) \\(Return 0 if they both go over\\)").matcher(query);
		if (blackjackMatcher.matches()) {
			int n1 = Integer.valueOf(blackjackMatcher.group(1));
			int n2 = Integer.valueOf(blackjackMatcher.group(2));

			if (n1 > 21 && n2 > 21) {
				return String.valueOf(0);
			} else if (n1 > 21) {
				return String.valueOf(n2);
			} else if (n2 > 21) {
				return String.valueOf(n1);
			}
			return String.valueOf(Math.max(n1, n2));
		}

		// FahrenheitToCelsiusQuestion
		Matcher fahrenheitToCelsiusMatcher = Pattern.compile("how much is (\\d+) Fahrenheit in Celsius \\(2 digits after decimal point\\)").matcher(query);
		if (fahrenheitToCelsiusMatcher.matches()) {
			int number = Integer.valueOf(fahrenheitToCelsiusMatcher.group(1));

			return String.format("%.2f", ((float) (number - 32) * 5 / 9));
		}

		// FeetToMetersQuestion
		Matcher feetToMeterMatcher = Pattern.compile("how much is (\\d+) feets in meters \\(2 digits after decimal point\\)").matcher(query);
		if (feetToMeterMatcher.matches()) {
			float result = 0.3048f * Integer.valueOf(feetToMeterMatcher.group(1));

			return String.format("%.2f", result);
		}

		// FibonacciQuestion
		Matcher fibonacciMatcher = Pattern.compile("what is the (\\d+)(?:st|nd|rd|th) number in the Fibonacci sequence").matcher(query);
		if (fibonacciMatcher.matches()) {
			int n = Integer.valueOf(fibonacciMatcher.group(1));
			return String.valueOf(fib(n));
		}

		// GeneralArithmeticQuestion
		Matcher generalArithmeticMatcher = Pattern.compile("what is ((\\d+)( (plus|minus|times) (\\d+))*)").matcher(query);
		if (generalArithmeticMatcher.matches()) {
			String arithmeticExpression = generalArithmeticMatcher.group(1);

			arithmeticExpression = arithmeticExpression.replace("plus", "+");
			arithmeticExpression = arithmeticExpression.replace("minus", "-");
			arithmeticExpression = arithmeticExpression.replace("times", "*");

			ScriptEngineManager manager = new ScriptEngineManager();
			ScriptEngine engine = manager.getEngineByName("js");
			Double result = null;
			try {
				result = (Double) engine.eval(arithmeticExpression);
			} catch (ScriptException e) {
				// Nothing to do
				extremeStartupLogger.info("Exception : {}", e.getMessage());
			}

			return String.valueOf(result.intValue());
		}

		// GeneralKnowledgeQuestion
		switch (query) {
		case "what does 'RTFM' stand for":
			return "Read The Fucking Manual";
		case "who counted to infinity twice":
			return "Chuck Norris";
		case "what is the answer to life, the universe and everything":
			return "42";
		case "who said 'Luke, I am your father'":
			return "Darth Vader";
		case "who is the Prime Minister of Great Britain":
			return "David Cameron";
		case "what colour is a banana":
			return "yellow";
		case "who played James Bond in the film Dr No":
			return "Sean Connery";
		case "what is the capital city of Botswana":
			return "Gaborone";
		case "What is that which in the morning goeth upon four feet; up on two feet in the afternoon; and in the Evening upon three?":
			return "Man";

		default:
			break;
		}

		// HexadecimalQuestion
		Matcher hexaAddition = Pattern.compile("what is the decimal value of 0x([\\da-f]+) plus 0x([\\da-f]+)").matcher(query);
		if (hexaAddition.matches()) {
			return String.valueOf(Integer.parseInt(hexaAddition.group(1), 16) + Integer.parseInt(hexaAddition.group(2), 16));
		}

		// MaxblockQuestion
		Matcher maxblockMatcher = Pattern
				.compile(
						"given the string : \"(.+)\", return the length of the largest \"block\" in the string \\(a block is a run of adjacent chars that are the same\\)")
				.matcher(query);
		if (maxblockMatcher.matches()) {
			String string = maxblockMatcher.group(1);
			return String.valueOf(longestSequence(string));
		}

		// MaximumQuestion
		Matcher maximumMatcher = Pattern.compile("which of the following numbers is the largest: (.+)").matcher(query);
		if (maximumMatcher.matches()) {
			Integer max = 0;
			for (String number : maximumMatcher.group(1).split(",")) {
				max = Math.max(max, Integer.valueOf(number));
			}
			return max.toString();
		}

		// MinusQuestion
		Matcher minusMatcher = Pattern.compile("what is (\\d+) - (\\d+)").matcher(query);
		if (minusMatcher.matches()) {
			return String.valueOf(Integer.valueOf(minusMatcher.group(1)) - Integer.valueOf(minusMatcher.group(2)));
		}

		// MultiplicationQuestion
		Matcher multiplicationMatcher = Pattern.compile("what is (\\d+) multiplied by (\\d+)").matcher(query);
		if (multiplicationMatcher.matches()) {
			return String.valueOf(Integer.valueOf(multiplicationMatcher.group(1)) * Integer.valueOf(multiplicationMatcher.group(2)));
		}

		// MultiplicationAdditionQuestion
		Matcher multiplicationAdditionMatcher = Pattern.compile("what is (\\d+) multiplied by (\\d+) plus (\\d+)").matcher(query);
		if (multiplicationAdditionMatcher.matches()) {
			return String.valueOf((Integer.valueOf(multiplicationAdditionMatcher.group(1)) * Integer.valueOf(multiplicationAdditionMatcher.group(2)))
					* Integer.valueOf(multiplicationAdditionMatcher.group(3)));
		}

		// MultQuestion
		Matcher multMatcher = Pattern.compile("what is (\\d+) \\* (\\d+)").matcher(query);
		if (multMatcher.matches()) {
			return String.valueOf(Integer.valueOf(multMatcher.group(1)) * Integer.valueOf(multMatcher.group(2)));
		}

		// PalindromeQuestion
		Matcher palindromeMatcher = Pattern.compile("is \"([\\w_]+)\" a palindrome \\? \\(yes or no\\)").matcher(query);
		if (palindromeMatcher.matches()) {
			String word = palindromeMatcher.group(1);
			String reverse = "";
			int length = word.length();

			for (int i = length - 1; i >= 0; i--) {
				reverse = reverse + word.charAt(i);
			}

			return word.equals(reverse) ? "yes" : "no";
		}

		// ParenBitQuestion
		Matcher parenBitMatcher = Pattern
				.compile(
						"given the string : \"(.+)\", compute a new string made of only of the parenthesis and their contents, so \"xyz\\(abc\\)123\" yields \"\\(abc\\)\"")
				.matcher(query);
		if (parenBitMatcher.matches()) {
			String string = parenBitMatcher.group(1);
			int index1 = string.indexOf('(');
			int index2 = string.indexOf(')') + 1;
			
			return string.substring(index1, index2);
		}

		// PiQuestion
		Matcher piMatcher = Pattern.compile("what is the (\\d+)(?:st|nd|rd|th) decimal of Pi").matcher(query);
		if (piMatcher.matches()) {
			Integer nthDecimal = Integer.valueOf(piMatcher.group(1));
			return "141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117".substring(nthDecimal - 1, nthDecimal);
		}

		// PlusQuestion
		Matcher plusMatcher = Pattern.compile("what is (\\d+) \\+ (\\d+)").matcher(query);
		if (plusMatcher.matches()) {
			return String.valueOf(Integer.valueOf(plusMatcher.group(1)) + Integer.valueOf(plusMatcher.group(2)));
		}

		// PowerQuestion
		Matcher powerMatcher = Pattern.compile("what is (\\d+) to the power of (\\d+)").matcher(query);
		if (powerMatcher.matches()) {
			return String.valueOf((int) Math.pow(Integer.valueOf(powerMatcher.group(1)), Integer.valueOf(powerMatcher.group(2))));
		}

		// PrimesQuestion
		Matcher primesMatcher = Pattern.compile("which of the following numbers are primes: (.+)").matcher(query);
		if (primesMatcher.matches()) {
			List<Integer> correctAnswers = new ArrayList<>();

			for (String number : primesMatcher.group(1).split(",")) {
				int n = Integer.valueOf(number);
				if (isPrime(n)) {
					correctAnswers.add(n);
				}
			}
			return StringUtils.collectionToCommaDelimitedString(correctAnswers);
		}

		// ReverseNumberQuestion
		Matcher reverseNumberMatcher = Pattern.compile("reverse that number : (\\d+)").matcher(query);
		if (reverseNumberMatcher.matches()) {
			int numberToReverse = Integer.valueOf(reverseNumberMatcher.group(1));
			int n = numberToReverse;
			int reverse = 0;

			while (n != 0) {
				reverse = reverse * 10;
				reverse = reverse + n % 10;
				n = n / 10;
			}
			String number = String.valueOf(numberToReverse);
			return String.format("%0" + number.length() + "d", reverse);
		}

		// ScrabbleQuestion
		Matcher scrabbleMatcher = Pattern.compile("what is the english scrabble score of \"([\\w_]+)\"").matcher(query);
		if (scrabbleMatcher.matches()) {
			char englishScrabbleScore_[] = { 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };
			String word = scrabbleMatcher.group(1);
			char[] chars = word.toCharArray();
			int score = 0;
			for (char character : chars) {
				if (character >= 'a' && character <= 'z') {
					score += englishScrabbleScore_[character - 'a'];
				}
			}
			return String.valueOf(score);
		}

		// SHA1Question
		Matcher sha1Matcher = Pattern.compile("what is the sha1 for \"([\\w_]+)\"").matcher(query);
		if (sha1Matcher.matches()) {
			try {
				byte[] digest = MessageDigest.getInstance("SHA-1").digest(sha1Matcher.group(1).getBytes());
				StringBuilder sb = new StringBuilder();
				for (byte aByteDigest : digest) {
					String hex = Integer.toHexString(0xFF & aByteDigest);
					if (hex.length() == 1) {
						sb.append('0');
					}
					sb.append(hex);
				}
				return sb.toString();
			} catch (NoSuchAlgorithmException e) {
				extremeStartupLogger.info(e.getMessage());
			}
		}

		// SquareCubeQuestion
		Matcher squareCubeMatcher = Pattern.compile("which of the following numbers is both a square and a cube: (.+)").matcher(query);
		if (squareCubeMatcher.matches()) {
			List<Integer> correctAnswers = new ArrayList<>();

			for (String number : squareCubeMatcher.group(1).split(",")) {
				int n = Integer.valueOf(number);
				if (isSquare(n) && isCube(n)) {
					correctAnswers.add(n);
				}
			}
			return StringUtils.collectionToCommaDelimitedString(correctAnswers);
		}

		// SubtractionQuestion
		Matcher subtractionMatcher = Pattern.compile("what is (\\d+) minus (\\d+)").matcher(query);
		if (subtractionMatcher.matches()) {
			return String.valueOf(Integer.valueOf(subtractionMatcher.group(1)) - Integer.valueOf(subtractionMatcher.group(2)));
		}

		// WeekdayQuestion
		Matcher dayOfWeekMatcher = Pattern.compile("which day of the week is (.*)").matcher(query);
		if (dayOfWeekMatcher.matches()) {
			Date date = null;
			DateFormat sdf = new SimpleDateFormat("dd MM yyyy");
			try {
				date = sdf.parse(dayOfWeekMatcher.group(1));
			} catch (ParseException e) {
				// Nothing to do
				extremeStartupLogger.info("Exception : {}", e.getMessage());
			}
			DateFormat df = new SimpleDateFormat("EEEE");
			return df.format(date);
		}

		return "no match";
	}

	// /////////////////////

	private int longestSequence(String str) {
		if (str.length() == 0)
			return 0;
		char c = str.charAt(0);
		String subs = str.replaceAll("(" + c + "+).*", "$1");
		int susi = subs.length();
		int rest = longestSequence(str.substring(susi));
		if (susi >= rest) {
			return susi;
		}
		return rest;
	}

	/**
	 * Checks whether an int is prime or not.
	 * 
	 * @param n
	 * @return
	 */
	private boolean isPrime(int n) {
		if (n == 1 || n == 0)
			return false;
		if (n == 2)
			return true;
		// check if n is a multiple of 2
		if (n % 2 == 0)
			return false;
		// if not, then just check the odds
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	private long fib(int n) {
		if (n <= 1)
			return n;
		else
			return fib(n - 1) + fib(n - 2);
	}

	private boolean isSquare(double cube) {
		if (cube == 0) {
			return true;
		}
		return cube % (Math.sqrt(cube)) == 0;
	}

	private boolean isCube(double x) {
		if (x == 0) {
			return true;
		}
		return x % (Math.cbrt(x)) == 0;
	}

}
