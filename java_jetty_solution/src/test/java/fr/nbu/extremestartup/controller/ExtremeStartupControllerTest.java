package fr.nbu.extremestartup.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExtremeStartupControllerTest {

	private ExtremeStartupController extremeStartupController;

	@Before
	public void setup() {
		extremeStartupController = new ExtremeStartupController();
	}

	@Test
	public void testFahreinheitQuestion() {
		String query = "how much is 2 Fahrenheit in Celsius (2 digits after decimal point)";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("-16,67", result);
	}

	@Test
	public void testFibonacciQuestion1() {
		String query = "what is the 9th number in the Fibonacci sequence";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("34", result);
	}

	@Test
	public void testFibonacciQuestion2() {
		String query = "what is the 6th number in the Fibonacci sequence";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("8", result);
	}

	@Test
	public void testFeetToMetersQuestion1() {
		String query = "how much is 9 feets in meters (2 digits after decimal point)";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("2,74", result);
	}

	@Test
	public void testFeetToMetersQuestion2() {
		String query = "how much is 8 feets in meters (2 digits after decimal point)";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("2,44", result);
	}

	@Test
	public void testPalindromeQuestion() {
		String query = "is \"bigdata\" a palindrome ? (yes or no)";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("no", result);
	}

	@Test
	public void testWeekdayQuestion() {
		String query = "which day of the week is 07 03 2014";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("vendredi", result);
	}

	@Test
	public void testPrimeQuestion1() {
		String query = "which of the following numbers are primes: 25,80,4423,9467";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("4423,9467", result);
	}

	@Test
	public void testPrimeQuestion2() {
		String query = "which of the following numbers are primes: 1,99,1787,8093";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("1787,8093", result);
	}

	@Test
	public void testPlusQuestion() {
		String query = "what is 15 %2B 9";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("24", result);
	}

	@Test
	public void testGeneralArithmeticQuestion() {
		String query = "what is 13 times 26 minus 36";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("302", result);
	}

	@Test
	public void testSubtractionQuestion() {
		String query = "what is 14 minus 1";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("13", result);
	}

	@Test
	public void testSquareCubeQuestion1() {
		String query = "which of the following numbers is both a square and a cube: 15625,117649,32,15";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("15625,117649", result);
	}

	@Test
	public void testSquareCubeQuestion2() {
		String query = "which of the following numbers is both a square and a cube: 4826809,1,97,47";

		String result = extremeStartupController.handler(query);

		Assert.assertEquals("4826809,1", result);
	}

}
