$(document).ready(function() {

	$('#nextround').click(function(){
		$.post('/nextround', function(data) {
		  location.reload();
		});
	});

	$('#pause').click(function(){
 		$.post('/pause', function(data) {
		  location.reload();
		});
	});

	$('#resume').click(function(){
 		$.post('/resume', function(data) {
		  location.reload();
		});
	});
	
});