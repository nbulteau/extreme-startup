package fr.nbu.extremestartup.server.domain;

public class LeaderBoardEntry {

    private final String playerId;

    private final String playerName;

    private final int score;

    public LeaderBoardEntry(String playerId, String playerName, int score) {
        super();
        this.playerId = playerId;
        this.playerName = playerName;
        this.score = score;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public int getScore() {
        return score;
    }
}
