package fr.nbu.extremestartup.server.domain.question.ternarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class MultiplicationAdditionQuestion extends TernaryMathsQuestion {

    public MultiplicationAdditionQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is " + n1 + " multiplied by " + n2 + " plus " + n3;
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(n1 * n2 + n3);
    }

    @Override
    public int getPoints() {
        return 50;
    }

}
