package fr.nbu.extremestartup.server.core;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public interface QuestionFactory {

    Question nextQuestion(Player player);

    void advanceRound();

    int getRound();
}