package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class PiQuestion extends UnaryMathsQuestion {

    private static final String DECIMAL_OF_PI = "1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679";

    public PiQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is the " + (random + 1) + th(random) + " decimal of Pi";
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(DECIMAL_OF_PI.charAt(random));
    }

    private String th(int number) {
        int mod = number % 10;

        switch (mod) {
        case 1:
            return "st";
        case 2:
            return "nd";
        case 3:
            return "rd";
        default:
            return "th";
        }
    }
}
