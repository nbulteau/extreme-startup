package fr.nbu.extremestartup.server.core;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.WarmupQuestion;

public class WarmupQuestionFactory implements QuestionFactory {

    @Override
    public Question nextQuestion(Player player) {
        return new WarmupQuestion(player);
    }

    @Override
    public void advanceRound() {
        // nothing to do
    }

    @Override
    public int getRound() {
        return 0;
    }

}
