package fr.nbu.extremestartup.server.domain.question.string;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class PalindromeQuestion extends Question {

	private final String[] buzzwords = { "madam", "dammit", "deified", "kayak", "desserts", "riddle" };

	private final String word;

	public PalindromeQuestion(Player player) {
		super(player);
		word = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();
	}

	@Override
	public String asText() {
		return "is \"" + word + "\" a palindrome ? (yes or no)";
	}

	@Override
	public String correctAnswer() {
		StringBuilder reverse = new StringBuilder();
		int length = word.length();

		for (int i = length - 1; i >= 0; i--) {
			reverse.append(word.charAt(i));
		}

		return word.equals(reverse.toString()) ? "yes" : "no";
	}

}
