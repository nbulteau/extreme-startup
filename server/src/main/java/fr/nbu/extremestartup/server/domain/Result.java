package fr.nbu.extremestartup.server.domain;

public enum Result {
    CORRECT, WRONG, ERROR_RESPONSE, NO_SERVER_RESPONSE;
}
