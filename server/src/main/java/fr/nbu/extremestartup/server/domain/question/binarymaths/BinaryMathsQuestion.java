package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public abstract class BinaryMathsQuestion extends Question {

	protected int n1;

	protected int n2;

	public BinaryMathsQuestion(final Player player) {
		super(player);
		this.n1 = random(1, 22);
		this.n2 = random(1, 22);
	}

	public BinaryMathsQuestion(final Player player, final int n1, final int n2) {
		super(player);
		this.n1 = n1;
		this.n2 = n2;
	}

}
