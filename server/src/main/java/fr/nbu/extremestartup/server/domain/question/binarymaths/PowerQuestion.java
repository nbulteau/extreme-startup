package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class PowerQuestion extends BinaryMathsQuestion {

	public PowerQuestion(final Player player) {
		super(player);
		this.n2 = random(1, 8);
	}

	public PowerQuestion(final Player player, final int n1, final int n2) {
		super(player, n1, n2);
	}

	@Override
	public String asText() {
		return "what is " + n1 + " to the power of " + n2;
	}

	@Override
	public int getPoints() {
		return 20;
	}

	@Override
	public String correctAnswer() {
		return String.valueOf((int) Math.pow(n1, n2));
	}

}
