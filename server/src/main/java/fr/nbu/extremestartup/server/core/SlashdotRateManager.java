package fr.nbu.extremestartup.server.core;

import fr.nbu.extremestartup.server.domain.question.Question;


public class SlashdotRateManager extends RateManager {

    public SlashdotRateManager() {
        super();
        this.delay = 0.02f;
    }

    @Override
    public long delayBeforeNextRequest(Question question) {
        long result = (long) delay * 1000;
        delay = delay * 1.022f;
        return result;
    }

    @Override
    public RateManager updateAlgorithmBasedOnScore(int score) {
        if (delay > 3.5) {
            return new RateManager();
        }
        return this;
    }

}
