package fr.nbu.extremestartup.server.domain.question.string;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class ParenBitQuestion extends Question {

    private final String[] buzzwords = { "this (is a) test", "()",
            "zyxw(abcd)54321", "123(riddle)", "(kayak)", "(bazinga)desserts",
            "12345(monster)abc" };

    private final String string;

    public ParenBitQuestion(final Player player) {
        super(player);
        string = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();
    }

    public ParenBitQuestion(final Player player, final String string) {
        super(player);
        this.string = string;
    }

    @Override
    public String asText() {
        return "given the string : \""
                + string
                + "\", compute a new string made of only of the parenthesis and their contents, so \"xyz(abc)123\" yields \"(abc)\"";
    }

    @Override
    public String correctAnswer() {
        int index1 = string.indexOf('(');
        int index2 = string.indexOf(')') + 1;

        return string.substring(index1, index2);
    }

}
