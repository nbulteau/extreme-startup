package fr.nbu.extremestartup.server.domain.question.miscellaneous;

public class QuestionAnswer {
    private final String question;

    private final String answer;

    public QuestionAnswer(String question, String answer) {
        super();
        this.question = question;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }
}
