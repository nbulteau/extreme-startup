package fr.nbu.extremestartup.server.domain.question;

import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.Result;

/**
 * Define an abstract question for Extreme Startup
 * 
 * @author Nicolas
 * 
 */
public abstract class Question {

	protected static final Logger LOGGER = LoggerFactory.getLogger("eXtremeStartup");

	/**
	 * Provides base value of this question disregarding actual answer if
	 * defined
	 */
	public static final int BASE_POINT = 10;

	private final UUID uuid = UUID.randomUUID();

	protected final Player player;

	private String answer;

	private Result result;

	private long duration;

	public Question(Player player) {
		super();
		this.player = player;
	}

	public abstract String asText();

	public abstract String correctAnswer();

	public void ask(int timeout) {

		RestTemplate restTemplate = buildRestTemplate(timeout);
		try {
			String url = player.getUrl() + "?q=" + URLEncoder.encode(this.asText(), "UTF-8");

			String response = restTemplate.getForObject(url, String.class);
			if (response != null) {
				answer = response.toLowerCase().trim();
				if (answeredCorrectly()) {
					result = Result.CORRECT;
				} else {
					result = Result.WRONG;
					LOGGER.info(displayResult());
				}
			} else {
				result = Result.ERROR_RESPONSE;
				LOGGER.info(displayResult());
			}
		} catch (RestClientException rce) {
			result = Result.ERROR_RESPONSE;
			LOGGER.info(displayResult());
		} catch (Exception e) {
			result = Result.NO_SERVER_RESPONSE;
			LOGGER.info(displayResult());
		}
	}

	public UUID getUuid() {
		return uuid;
	}

	public int getPoints() {
		return BASE_POINT;
	}

	public int getDelayBeforeNext() {
		switch (result) {
		case CORRECT:
			return 5;
		case WRONG:
			return 10;
		default:
			return 20;
		}
	}

	public String displayResult() {
		return "question:" + this.asText() + " answer: " + answer + " result: " + result;
	}

	public void setAnswer(String answer) {
		this.answer = answer.toLowerCase().trim();
	}

	public String getAnswer() {
		return answer;
	}

	public Result getResult() {
		return result;
	}

	public boolean wasAnsweredCorrectly() {
		return result == Result.CORRECT;
	}

	public boolean wasAnsweredWrongly() {
		return result == Result.WRONG;
	}

	@Override
	public String toString() {
		return "Question [" + uuid + "]";
	}

	protected static int random(int min, int max) {
		Random random = new Random();
		return random.nextInt(max) + min;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	private boolean answeredCorrectly() {
		return correctAnswer().trim().equalsIgnoreCase(answer);
	}

	private RestTemplate buildRestTemplate(int timeout) {
		RestTemplate restTemplate = new RestTemplate();
		if (restTemplate.getRequestFactory() instanceof SimpleClientHttpRequestFactory) {
			((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(timeout * 1000);
			((SimpleClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(timeout * 1000);
		} else if (restTemplate.getRequestFactory() instanceof HttpComponentsClientHttpRequestFactory) {
			((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(timeout * 1000);
			((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(timeout * 1000);
		}

		return restTemplate;
	}

}
