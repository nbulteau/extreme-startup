package fr.nbu.extremestartup.server.domain.question.miscellaneous;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

/**
 * Generates arbitrary arithmetic questions with 'plus', 'times' and 'minus'
 * operators.
 * 
 * Generated expression should be evaluated from left to right respecting usual
 * precedence of operators: * binds tighter than +.
 * 
 * This means the expression 12 + 3 * 4 + 5 evaluates to 29 and not 65.
 * 
 * @author Nicolas
 * 
 */
public class GeneralArithmeticQuestion extends Question {

	private static final String[] OPERATORS = { "plus", "minus", "times" };

	private final List<String> tokens = generateExpression();

	public GeneralArithmeticQuestion(Player player) {
		super(player);
	}

	@Override
	public String asText() {
		return "what is " + displayExpression();
	}

	private String displayExpression() {
		StringBuilder expression = new StringBuilder();
		for (String token : tokens) {
			expression.append(token).append(" ");
		}
		return expression.toString().trim();
	}

	private String expression() {
		StringBuilder expression = new StringBuilder();
		for (String token : tokens) {
			switch (token) {
			case "plus":
				expression.append(" + ");
				break;
			case "minus":
				expression.append(" - ");
				break;
			case "times":
				expression.append(" * ");
				break;
			default:
				expression.append(token);
				break;
			}
		}
		return expression.toString();
	}

	@Override
	public String correctAnswer() {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		Double result = null;
		try {
			result = (Double) engine.eval(expression());
		} catch (ScriptException e) {
			// Nothing to do
			LOGGER.info("Exception in GeneralArithmeticQuestion : {}", e.getMessage());
		}
		if (result != null) {
			return String.valueOf(result.intValue());
		} else {
			LOGGER.info("result == null");
			return "";
		}
	}

	private final List<String> generateExpression() {
		List<String> expression = new ArrayList<>();
		int random = random(1, 5);
		for (int i = 0; i < random; i++) {
			expression.add(String.valueOf(random(1, 50)));
			expression.add(OPERATORS[random(0, 3)]);
		}
		expression.add(String.valueOf(random(1, 50)));
		return expression;
	}

	@Override
	public int getPoints() {
		return 60;
	}

}
