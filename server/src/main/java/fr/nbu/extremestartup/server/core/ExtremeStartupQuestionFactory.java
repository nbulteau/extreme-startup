package fr.nbu.extremestartup.server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;
import fr.nbu.extremestartup.server.domain.question.binarymaths.AdditionQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.BlackjackQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.HexadecimalQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.MinusQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.MultQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.MultiplicationQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.PlusQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.PowerQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.SubtractionQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.GeneralArithmeticQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.GeneralKnowledgeQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.WeekdayQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.MaximumQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.PrimesQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.SquareCubeQuestion;
import fr.nbu.extremestartup.server.domain.question.string.AlphagramQuestion;
import fr.nbu.extremestartup.server.domain.question.string.MaxblockQuestion;
import fr.nbu.extremestartup.server.domain.question.string.PalindromeQuestion;
import fr.nbu.extremestartup.server.domain.question.string.ParenBitQuestion;
import fr.nbu.extremestartup.server.domain.question.string.SHA1Question;
import fr.nbu.extremestartup.server.domain.question.string.ScrabbleQuestion;
import fr.nbu.extremestartup.server.domain.question.ternarymaths.AdditionAdditionQuestion;
import fr.nbu.extremestartup.server.domain.question.ternarymaths.AdditionMultiplicationQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FahrenheitToCelsiusQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FeetToMetersQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FibonacciQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.PiQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.ReverseNumberQuestion;

/**
 * An eXtreme Startup factory
 * 
 */
public class ExtremeStartupQuestionFactory implements QuestionFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger("eXtremeStartup");

	private int round = 1;

	@Override
	public Question nextQuestion(final Player player) {
		int higher = round * 2 - 1;
		if (higher > QuestionType.values().length) {
			higher = QuestionType.values().length;
		}
		int lower = Math.max(0, higher - 4);
		int random = (int) (Math.random() * (higher - lower)) + lower;

		QuestionType questionType = QuestionType.values()[random];

		LOGGER.debug("round : {}, lower : {}, higher : {}, question type : {}", round, lower, higher, questionType);

		return createQuestion(player, questionType);
	}

	@Override
	public void advanceRound() {
		round++;
	}

	@Override
	public int getRound() {
		return round;
	}

	public Question createQuestion(final Player player, final QuestionType questionType) {
		Question question = null;

		switch (questionType) {
		case ADDITION_QUESTION:
			question = new AdditionQuestion(player);
			break;
		case ADDITION_ADDITION_QUESTION:
			question = new AdditionAdditionQuestion(player);
			break;
		case ADDITION_MULTIPLICATION_QUESTION:
			question = new AdditionMultiplicationQuestion(player);
			break;
		case ALPHAGRAM_QUESTION:
			question = new AlphagramQuestion(player);
			break;
		case BLACKJACK_QUESTION:
			question = new BlackjackQuestion(player);
			break;
		case MULTIPLICATION_ADDITION_QUESTION:
			question = new AdditionMultiplicationQuestion(player);
			break;
		case FAHRENHEIT_TO_CELSIUS_QUESTION:
			question = new FahrenheitToCelsiusQuestion(player);
			break;
		case FEET_TO_METERS_QUESTION:
			question = new FeetToMetersQuestion(player);
			break;
		case FIBONACCI_QUESTION:
			question = new FibonacciQuestion(player);
			break;
		case GENERAL_ARITHMETIC_QUESTION:
			question = new GeneralArithmeticQuestion(player);
			break;
		case GENERAL_KNOWLEDGE_QUESTION:
			question = new GeneralKnowledgeQuestion(player);
			break;
		case HEXADECIMAL_QUESTION:
			question = new HexadecimalQuestion(player);
			break;
		case MAXBLOCK_QUESTION:
			question = new MaxblockQuestion(player);
			break;
		case MAXIMUM_QUESTION:
			question = new MaximumQuestion(player);
			break;
		case MINUS_QUESTION:
			question = new MinusQuestion(player);
			break;
		case MULTIPLICATION_QUESTION:
			question = new MultiplicationQuestion(player);
			break;
		case MULT_QUESTION:
			question = new MultQuestion(player);
			break;
		case PALINDROME_QUESTION:
			question = new PalindromeQuestion(player);
			break;
		case PARENTBIT_QUESTION:
			question = new ParenBitQuestion(player);
			break;
		case PI_QUESTION:
			question = new PiQuestion(player);
			break;
		case PLUS_QUESTION:
			question = new PlusQuestion(player);
			break;
		case POWER_QUESTION:
			question = new PowerQuestion(player);
			break;
		case PRIMES_QUESTION:
			question = new PrimesQuestion(player);
			break;
		case REVERSE_NUMBER_QUESTION:
			question = new ReverseNumberQuestion(player);
			break;
		case SCRABBLE_QUESTION:
			question = new ScrabbleQuestion(player);
			break;
		case SHA1_QUESTION:
			question = new SHA1Question(player);
			break;
		case SQUARE_CUBE_QUESTION:
			question = new SquareCubeQuestion(player);
			break;
		case SUBTRACTION_QUESTION:
			question = new SubtractionQuestion(player);
			break;
		case WEEKDAY_QUESTION:
			question = new WeekdayQuestion(player);
			break;
		default:
			break;
		}
		return question;
	}

}
