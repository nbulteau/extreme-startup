package fr.nbu.extremestartup.server.core.logger;

import static net.logstash.logback.marker.Markers.appendEntries;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.nbu.extremestartup.server.core.QuestionFactory;
import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

/**
 * An object exposing all the registrable and replayable events for the game as
 * methods. EventWriter is intended to be used for generating a replayable trace
 * of an eXtremeSTartup session, in order to be able to save, pause and restore
 * a game at a known state.
 * 
 * @author Nicolas
 * 
 */
public class EventWriter {

	private static final Logger LOGGER = LoggerFactory.getLogger("EventWriter");

	/**
	 * A player has just registered for the game and has started playing
	 * 
	 * @param player
	 */
	public static void logPlayerStarted(final Player player) {
		if (null != player) {
			Map<String, String> map = new HashMap<>();
			map.put("name", player.getName());
			map.put("url", player.getUrl());
			map.put("uuid", player.getUuid().toString());

			LOGGER.info(appendEntries(map), "player_started");
		}
	}

	/**
	 * A player has just registered for the game and has started playing
	 * 
	 * @param player
	 */
	public static void logPlayerWithdraw(final Player player) {
		if (null != player) {
			Map<String, String> map = new HashMap<>();
			map.put("name", player.getName());
			map.put("url", player.getUrl());
			map.put("uuid", "" + player.getUuid());

			LOGGER.info(appendEntries(map), "player_withdraw");
		}
	}

	/**
	 * Round has been advanced
	 * 
	 * @param questionFactory
	 *            : Question factory object advancing round
	 */
	public static void logAdvanceRound(final QuestionFactory questionFactory) {
		if (null != questionFactory) {
			Map<String, String> map = new HashMap<>();
			map.put("round", "" + questionFactory.getRound());

			LOGGER.info(appendEntries(map), "advance_round");
		}
	}

	/**
	 * A question has been asked and answered, possibly with an error
	 * 
	 * @param player
	 * @param question
	 */
	public static void logQuestion(final Player player, final Question question) {
		if (null != player && null != question) {
			Map<String, String> map = new HashMap<>();
			map.put("uuid", "" + player.getUuid());
			map.put("question", question.asText());
			map.put("answer", question.getAnswer());
			map.put("result", "" + question.getResult());
			map.put("duration", "" + question.getDuration());

			LOGGER.info(appendEntries(map), "question");
		}
	}

	/**
	 * Increment score of a player
	 * 
	 * @param player
	 * @param increment
	 * @param newScore
	 */
	public static void logIncrementScore(final Player player, final int increment, final int newScore) {
		if (player != null) {
			Map<String, String> map = new HashMap<>();
			map.put("name", player.getName());
			map.put("uuid", "" + player.getUuid());
			map.put("increment", "" + increment);
			map.put("newScore", "" + newScore);

			LOGGER.info(appendEntries(map), "score");
		}
	}
}
