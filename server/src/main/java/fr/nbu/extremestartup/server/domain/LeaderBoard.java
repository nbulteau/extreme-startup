package fr.nbu.extremestartup.server.domain;

import java.util.List;

public class LeaderBoard {

    private final boolean inplay;

    private final List<LeaderBoardEntry> leaderBoardEntries;

    public LeaderBoard(boolean inplay, List<LeaderBoardEntry> leaderBoardEntries) {
        super();
        this.inplay = inplay;
        this.leaderBoardEntries = leaderBoardEntries;
    }

    public boolean isInplay() {
        return inplay;
    }

    public List<LeaderBoardEntry> getLeaderBoardEntries() {
        return leaderBoardEntries;
    }

}
