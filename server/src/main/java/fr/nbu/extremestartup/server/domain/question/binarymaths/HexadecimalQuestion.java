package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class HexadecimalQuestion extends BinaryMathsQuestion {

    public HexadecimalQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return String.format("what is the decimal value of 0x%x plus 0x%x", n1,
                n2);
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(n1 + n2);
    }

}
