package fr.nbu.extremestartup.server.domain.question.selectfromlist;

import java.util.Collections;

import org.springframework.util.StringUtils;

import fr.nbu.extremestartup.server.domain.Player;

public class MaximumQuestion extends SelectFromListOfNumbersQuestion {

    public MaximumQuestion(Player player) {
        super(player);
    }

    @Override
    protected boolean shouldBeSelected(int x) {
        return x == Collections.max(numbers);
    }

    @Override
    protected Integer candidateNumber() {
        return random(1, 100);
    }

    @Override
    public String asText() {
        return "which of the following numbers is the largest: "
                + StringUtils.collectionToCommaDelimitedString(numbers);
    }

    @Override
    public int getPoints() {
        return 40;
    }
}
