package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class ReverseNumberQuestion extends Question {

    protected final int random;

    public ReverseNumberQuestion(Player player) {
        super(player);
        this.random = random(1100, 95820);
    }

    @Override
    public String asText() {
        return "reverse that number : " + random;
    }

    @Override
    public String correctAnswer() {
        int n = random;
        int reverse = 0;

        while (n != 0) {
            reverse = reverse * 10;
            reverse = reverse + n % 10;
            n = n / 10;
        }
        String number = String.valueOf(random);
        return String.format("%0" + number.length() + "d", reverse);
    }

}
