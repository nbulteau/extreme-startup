package fr.nbu.extremestartup.server.domain.question.selectfromlist;

import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;

import fr.nbu.extremestartup.server.domain.Player;

public class SquareCubeQuestion extends SelectFromListOfNumbersQuestion {

    private static final Set<Integer> CANDIDATE_NUMBERS = new HashSet<>();

    // setup CANDIDATE_NUMBERS
    static {
        for (int i = 1; i < 500; i++) {
            double cube = Math.pow(i, 3);
            if (SquareCubeQuestion.isSquare(cube)) {
                CANDIDATE_NUMBERS.add((int) cube);
            }
        }
    }

    public SquareCubeQuestion(Player player) {
        super(player);

    }

    @Override
    protected boolean shouldBeSelected(int x) {
        return isSquare(x) && isCube(x);
    }

    @Override
    protected Integer candidateNumber() {
        return (Integer) CANDIDATE_NUMBERS.toArray()[random(1,
                CANDIDATE_NUMBERS.size())];
    }

    @Override
    public String asText() {
        return "which of the following numbers is both a square and a cube: "
                + StringUtils.collectionToCommaDelimitedString(numbers);
    }

    @Override
    public int getPoints() {
        return 60;
    }

    private static boolean isSquare(double cube) {
        if (cube == 0) {
            return true;
        }
        return cube % (Math.sqrt(cube)) == 0;
    }

    private static boolean isCube(double x) {
        if (x == 0) {
            return true;
        }
        return x % (Math.cbrt(x)) == 0;
    }

}
