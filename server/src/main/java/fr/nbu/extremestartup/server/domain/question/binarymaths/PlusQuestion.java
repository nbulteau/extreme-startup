package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class PlusQuestion extends BinaryMathsQuestion {

    public PlusQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is " + n1 + " + " + n2;
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(n1 + n2);
    }
}
