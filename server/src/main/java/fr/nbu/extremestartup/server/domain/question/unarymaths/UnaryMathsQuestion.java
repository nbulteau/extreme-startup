package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public abstract class UnaryMathsQuestion extends Question {

    protected final int random;

    public UnaryMathsQuestion(Player player) {
        super(player);
        this.random = random(1, 20);
    }
}
