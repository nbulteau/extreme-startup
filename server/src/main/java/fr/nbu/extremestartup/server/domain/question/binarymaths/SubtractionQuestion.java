package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class SubtractionQuestion extends BinaryMathsQuestion {

    public SubtractionQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is " + n1 + " minus " + n2;
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(n1 - n2);
    }
}
