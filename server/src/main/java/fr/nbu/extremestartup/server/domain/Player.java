package fr.nbu.extremestartup.server.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Player {

    private final UUID uuid;

    private String name;

    private String url;

    private final List<LogLine> logs = new ArrayList<>();

    public Player() {
        super();
        uuid = UUID.randomUUID();
    }

    public Player(String name, String url) {
        super();
        uuid = UUID.randomUUID();
        this.name = name;
        this.url = url;
    }

    public void logResult(String answer, String msg, int points) {
        // append
        logs.add(0, new LogLine(answer, msg, points, new Date()));
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Player [name=" + name + ", url=" + url + "]";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<LogLine> getLogs() {
        // subList pour la presentation
        List<LogLine> subList;
        if (logs.size() < 200) {
            subList = logs.subList(0, logs.size());
        } else {
            subList = logs.subList(0, 200);
        }
        return subList;
    }

}
