package fr.nbu.extremestartup.server.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import fr.nbu.extremestartup.server.core.logger.EventWriter;
import fr.nbu.extremestartup.server.domain.LeaderBoard;
import fr.nbu.extremestartup.server.domain.LeaderBoardEntry;
import fr.nbu.extremestartup.server.domain.Player;

/**
 * The eXtreme Startup ServerLauncher
 * 
 * @author Nicolas
 * 
 */
public class ExtremeStartupServer {

    private final Map<UUID, Player> players = new HashMap<>();

    private final Map<UUID, Thread> playersThreads = new HashMap<>();

    private final GameState gameState;

    private final Scoreboard scoreboard;

    private final QuestionFactory questionFactory;

    public ExtremeStartupServer(final GameState gameState, final Scoreboard scoreboard,
    		final QuestionFactory questionFactory) {
        super();
        this.gameState = gameState;
        this.scoreboard = scoreboard;
        this.questionFactory = questionFactory;
    }

    public void addPlayer(final Player player) {
        scoreboard.newPlayer(player);
        players.put(player.getUuid(), player);
        QuizMaster quizMaster = new QuizMaster(player, scoreboard,
                questionFactory, gameState);
        playersThreads.put(player.getUuid(), quizMaster);
        quizMaster.start();

        EventWriter.logPlayerStarted(player);
    }

    /**
     * Build the LeaderBoard
     * 
     * @return
     */
    public LeaderBoard scores() {
        List<LeaderBoardEntry> leaderBoardEntries = new ArrayList<>();
        Map<UUID, Integer> leaderBoard = scoreboard.getLeaderboard();
        SortedMap<UUID, Integer> sortedMap = Collections.synchronizedSortedMap(new TreeMap<UUID, Integer>(leaderBoard));
        
        
        Iterator<Entry<UUID, Integer>> iterator = sortedMap.entrySet()
                .iterator();
        while (iterator.hasNext()) {
            Map.Entry<UUID, Integer> entry = iterator.next();
            UUID key = entry.getKey();
            Integer score = entry.getValue();
            Player player = players.get(key);
            if (player != null) {
                leaderBoardEntries.add(new LeaderBoardEntry(key.toString(),
                        player.getName(), score));
            }
        }

        return new LeaderBoard(gameState.isRunning(), leaderBoardEntries);
    }

    public void advanceRound() {
        if (questionFactory.getRound() * 2 - 1 <= QuestionType.values().length) {
            questionFactory.advanceRound();
            EventWriter.logAdvanceRound(questionFactory);
        }
    }

    public int getRound() {
        return questionFactory.getRound();
    }

    public void pause() {
        gameState.pause();
    }

    public void resume() {
        gameState.resume();
    }

    public boolean isRunning() {
        return gameState.isRunning();
    }

    public Player getPlayer(final UUID uuid) {
        Player player = null;
        if (uuid != null) {
            player = players.get(uuid);
        }
        return player;
    }

    public void deletePlayer(final UUID uuid) {
        if (uuid != null) {
            Player player = players.remove(uuid);
            if (player != null) {
                EventWriter.logPlayerWithdraw(player);

                playersThreads.get(uuid).interrupt();
                playersThreads.remove(uuid);
            }
        }
    }

    public int getScore(final Player player) {
        return scoreboard.currentScore(player);
    }

}
