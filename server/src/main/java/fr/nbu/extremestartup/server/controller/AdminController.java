package fr.nbu.extremestartup.server.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.nbu.extremestartup.server.core.ExtremeStartupServer;
import fr.nbu.extremestartup.server.domain.LeaderBoard;

@RestController
public class AdminController {

    ExtremeStartupServer extremeStartupServer;

    @Autowired
    public AdminController(ExtremeStartupServer extremeStartupServer) {
        super();
        this.extremeStartupServer = extremeStartupServer;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/pause")
    public void pause() {
        extremeStartupServer.pause();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/resume")
    public void resume() {
        extremeStartupServer.resume();
    }

    @RequestMapping(value = "/nextround")
    public void nextround() {
        extremeStartupServer.advanceRound();
    }

    @RequestMapping(value = "/scores")
    public LeaderBoard scores(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        return extremeStartupServer.scores();
    }
}
