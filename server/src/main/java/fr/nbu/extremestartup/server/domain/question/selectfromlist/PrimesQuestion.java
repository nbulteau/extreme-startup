package fr.nbu.extremestartup.server.domain.question.selectfromlist;

import java.util.Arrays;

import org.springframework.util.StringUtils;

import fr.nbu.extremestartup.server.domain.Player;

public class PrimesQuestion extends SelectFromListOfNumbersQuestion {

    // will contain true or false values for the first 10,000 integers
    private static final boolean[] PRIMES = new boolean[10000];

    // setup PRIMES
    static {
        // assume all integers are prime.
        Arrays.fill(PRIMES, true);
        // we know 0 and 1 are not prime.
        PRIMES[0] = PRIMES[1] = false;
        for (int i = 2; i < PRIMES.length; i++) {
            // if the number is prime,
            // then go through all its multiples and make their values false.
            if (PRIMES[i]) {
                for (int j = 2; i * j < PRIMES.length; j++) {
                    PRIMES[i * j] = false;
                }
            }
        }
    }

    public PrimesQuestion(Player player) {
        super(player);
    }

    @Override
    protected boolean shouldBeSelected(int x) {
        return PRIMES[x];
    }

    @Override
    protected Integer candidateNumber() {
        while (true) {
            int random = random(1, 10000);
            if (PRIMES[random]) {
                return random;
            }
        }
    }

    @Override
    public String asText() {
        return "which of the following numbers are primes: "
                + StringUtils.collectionToCommaDelimitedString(numbers);
    }

    @Override
    public int getPoints() {
        return 60;
    }

}
