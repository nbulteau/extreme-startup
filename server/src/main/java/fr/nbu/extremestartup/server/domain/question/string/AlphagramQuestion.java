package fr.nbu.extremestartup.server.domain.question.string;

import java.util.Arrays;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class AlphagramQuestion extends Question {

	private final String[] buzzwords = { "mongodb", "cloud", "bigdata", "yottabytes", "bazinga", "virtualisation" };

	private final String word;

	public AlphagramQuestion(final Player player) {
		super(player);
		this.word = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();
	}

	public AlphagramQuestion(final Player player, final String word) {
		super(player);
		this.word = word;
	}

	@Override
	public String asText() {
		return "what is the Alphagram of \"" + word + "\"";
	}

	@Override
	public String correctAnswer() {
		char[] letters = word.toCharArray();
		Arrays.sort(letters);
		return new String(letters);
	}

}
