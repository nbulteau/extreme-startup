package fr.nbu.extremestartup.server.core;

public class GameState {

    private boolean paused = false;

    public GameState() {
        super();
    }

    public boolean isRunning() {
        return !paused;
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }

}
