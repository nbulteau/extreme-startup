package fr.nbu.extremestartup.server.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.nbu.extremestartup.server.core.ExtremeStartupServer;
import fr.nbu.extremestartup.server.core.QuestionType;
import fr.nbu.extremestartup.server.domain.LeaderBoardEntry;
import fr.nbu.extremestartup.server.domain.Player;

@Controller
public class ExtremeStartupController {

    private static final String[] HEADERS_TO_TRY = { "X-Forwarded-For",
            "Proxy-Client-IP", "WL-Proxy-Client-IP", "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR" };

    private static String getClientIpAddress(HttpServletRequest request) {
        for (String header : HEADERS_TO_TRY) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0
                    && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }

    private ExtremeStartupServer extremeStartupServer;

    @Autowired
    public ExtremeStartupController(ExtremeStartupServer extremeStartupServer) {
        super();
        this.extremeStartupServer = extremeStartupServer;
    }

    @ModelAttribute("leaderBoardEntries")
    public List<LeaderBoardEntry> leaderBoard() {
        return this.extremeStartupServer.scores().getLeaderBoardEntries();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String index(final ModelMap model,
            HttpServletRequest httpServletRequest) {
        try {
            InetAddress ipAddress = InetAddress.getLocalHost();
            model.addAttribute("ipServer", ipAddress);
        } catch (UnknownHostException exception) {
            exception.printStackTrace();
        }
        model.addAttribute("ipClient", getClientIpAddress(httpServletRequest));

        return "leaderboard";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/player")
    public String player(final Player player) {
        return "player";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/player")
    public String player(final Player player,
            final BindingResult bindingResult, final ModelMap model) {

        validatePlayer(player, bindingResult);

        if (bindingResult.hasErrors()) {
            return "player";
        }

        extremeStartupServer.addPlayer(player);

        return "playerAdded";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/player/{uuid}")
    public String player(@PathVariable UUID uuid, final ModelMap model) {
        Player player = extremeStartupServer.getPlayer(uuid);
        if (player != null) {
            model.addAttribute("player", player);
            int score = extremeStartupServer.getScore(player);
            model.addAttribute("score", score);
        }
        return "personnalScore";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/withdraw/player/{uuid}")
    public String removePlayer(@PathVariable UUID uuid) {
        Player player = extremeStartupServer.getPlayer(uuid);
        if (player != null) {
            extremeStartupServer.deletePlayer(uuid);
        }
        return "leaderboard";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/controlpanel")
    public String controlpanel(final ModelMap model) {
        int round = extremeStartupServer.getRound();
        int higher = round * 2 - 1;
        if (higher > QuestionType.values().length) {
            higher = QuestionType.values().length;
        }
        int lower = Math.max(0, higher - 4);

        List<String> questions = new ArrayList<>();
        for (int i = lower; i < higher; i++) {
            questions.add(QuestionType.values()[i].name());
        }

        model.addAttribute("round", round);
        model.addAttribute("questions", questions);
        model.addAttribute("isRunning", extremeStartupServer.isRunning());

        return "controlpanel";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/graph")
    public String graph() {
        return "scores";
    }

    private void validatePlayer(final Player player,
            final BindingResult bindingResult) {
        if (player.getName() == null) {
            bindingResult
                    .addError(new ObjectError("name", "Name can't be null"));
        } else {
            if (player.getName().length() < 3) {
                bindingResult.addError(new ObjectError("name",
                        "Name is too short"));
            } else if (player.getName().length() > 30) {
                bindingResult.addError(new ObjectError("name",
                        "Name is too long"));
            }
        }
        if ("".equals(player.getUrl())) {
            bindingResult
                    .addError(new ObjectError("url", "URL can't be empty"));
        } else {
            UrlValidator urlValidator = new UrlValidator(
                    new String[] { "http" }, UrlValidator.ALLOW_LOCAL_URLS);
            if (!urlValidator.isValid(player.getUrl())) {
                bindingResult
                        .addError(new ObjectError("url", "URL is invalid"));
            }
        }
    }

}
