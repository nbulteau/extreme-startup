package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class FeetToMetersQuestion extends UnaryMathsQuestion {

    private static final float RATIO = 0.3048f;

    public FeetToMetersQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "how much is " + random
                + " feets in meters (2 digits after decimal point)";
    }

    @Override
    public int getPoints() {
        return 20;
    }

    @Override
    public String correctAnswer() {
        return String.format("%.2f", RATIO * (random));
    }

}
