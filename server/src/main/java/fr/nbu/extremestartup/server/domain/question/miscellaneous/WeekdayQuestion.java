package fr.nbu.extremestartup.server.domain.question.miscellaneous;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class WeekdayQuestion extends Question {

	private final Date date;

	public WeekdayQuestion(final Player player) {
		super(player);
		date = new Date(new Date().getTime() + random(10000000, 999999999));
	}

	public WeekdayQuestion(final Player player, final Date date) {
		super(player);
		this.date = date;
	}

	@Override
	public String asText() {
		final DateFormat sdf = new SimpleDateFormat("dd MM yyyy");
		return "which day of the week is " + sdf.format(date);
	}

	@Override
	public String correctAnswer() {
		final DateFormat df = new SimpleDateFormat("EEEE");
		return df.format(date);
	}

}
