package fr.nbu.extremestartup.server.domain.question.miscellaneous;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class WarmupQuestion extends Question {

    public WarmupQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is your name";
    }

    @Override
    public String correctAnswer() {
        return player.getName();
    }

}
