package fr.nbu.extremestartup.server.domain.question.string;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class ScrabbleQuestion extends Question {

	private static final char[] ENGLISH_SCRABBLE_SCORES = { 1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10 };

	private final String[] buzzwords = { "mongodb", "cloud", "bigdata", "yottabytes", "bazinga", "virtualisation" };

	private final String word;

	public ScrabbleQuestion(Player player) {
		super(player);
		word = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();

	}

	@Override
	public String asText() {
		return "what is the english scrabble score of \"" + word + "\"";
	}

	@Override
	public String correctAnswer() {
		char[] chars = word.toCharArray();
		int score = 0;
		for (char character : chars) {
			if (character >= 'a' && character <= 'z') {
				score += ENGLISH_SCRABBLE_SCORES[character - 'a'];
			}
		}
		return String.valueOf(score);
	}

}
