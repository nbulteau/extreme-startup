package fr.nbu.extremestartup.server.domain.question.binarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class BlackjackQuestion extends BinaryMathsQuestion {

	public BlackjackQuestion(Player player) {
		super(player, random(1, 30), random(1, 30));
	}

	public BlackjackQuestion(Player player, int n1, int n2) {
		super(player, n1, n2);
	}

	@Override
	public String asText() {
		return "return whichever value is nearest to 21 without going over : " + n1 + ", " + n2 + " (Return 0 if they both go over)";
	}

	@Override
	public String correctAnswer() {
		if (n1 > 21 && n2 > 21) {
			return String.valueOf(0);
		} else if (n1 > 21) {
			return String.valueOf(n2);
		} else if (n2 > 21) {
			return String.valueOf(n1);
		}
		return String.valueOf(Math.max(n1, n2));
	}

}
