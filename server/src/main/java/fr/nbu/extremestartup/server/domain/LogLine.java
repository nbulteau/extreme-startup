package fr.nbu.extremestartup.server.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogLine {
    private final String answer;
    private final String result;
    private final int points;
    private final String date;

    public LogLine(final String answer, final String result, final int points,
            final Date date) {
        super();
        this.answer = answer;
        this.result = result;
        this.points = points;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd 'at' HH:mm:ss");
        this.date = sdf.format(date);
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd 'at' HH:mm:ss");
        return "[answer=" + answer + ", result=" + result
                + " - points awarded : " + points + ", time : " + sdf.format(date)+ "]";
    }

    public String getAnswer() {
        return answer;
    }

    public String getResult() {
        return result;
    }

    public int getPoints() {
        return points;
    }

    public String getDate() {
        return date;
    }

}
