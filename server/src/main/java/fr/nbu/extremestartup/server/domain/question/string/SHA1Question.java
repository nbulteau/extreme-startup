package fr.nbu.extremestartup.server.domain.question.string;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class SHA1Question extends Question {

	private final String[] buzzwords = { "mongodb", "cloud", "bigdata", "yottabytes", "bazinga", "virtualisation" };

	private final String word;

	public SHA1Question(Player player) {
		super(player);
		word = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();
	}

	@Override
	public String asText() {
		return "what is the sha1 for \"" + word + "\"";
	}

	@Override
	public String correctAnswer() {
		try {
			byte[] digest = MessageDigest.getInstance("SHA-1").digest(word.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte aByteDigest : digest) {
				String hex = Integer.toHexString(0xFF & aByteDigest);
				if (hex.length() == 1) {
					sb.append('0');
				}
				sb.append(hex);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			LOGGER.info("Exception in a SHA1Question : {}", e.getMessage());
		}
		return null;
	}

}
