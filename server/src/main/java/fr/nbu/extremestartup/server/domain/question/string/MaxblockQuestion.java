package fr.nbu.extremestartup.server.domain.question.string;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class MaxblockQuestion extends Question {
	private final String[] buzzwords = { "hoopla", "aabbbcccBBBBddd", "kayak", "desserts", "XX112222BBBbbXX112222" };

	private final String string;

	public MaxblockQuestion(final Player player) {
		super(player);
		string = buzzwords[random(0, buzzwords.length - 1)].toLowerCase();
	}

	public MaxblockQuestion(final Player player, final String string) {
		super(player);
		this.string = string;
	}

	@Override
	public String asText() {
		return "given the string : \"" + string
				+ "\", return the length of the largest \"block\" in the string (a block is a run of adjacent chars that are the same)";
	}

	@Override
	public String correctAnswer() {
		return String.valueOf(longestSequence(string));
	}

	private int longestSequence(String str) {
		if (str.length() == 0)
			return 0;
		char c = str.charAt(0);
		String subs = str.replaceAll("(" + c + "+).*", "$1");
		int susi = subs.length();
		int rest = longestSequence(str.substring(susi));
		if (susi >= rest) {
			return susi;
		}
		return rest;
	}
}
