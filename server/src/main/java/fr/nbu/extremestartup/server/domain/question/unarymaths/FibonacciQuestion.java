package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;

/**
 * 
 * 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 10946
 * 17711 28657 46368 75025
 * 
 * @author Nicolas
 * 
 */
public class FibonacciQuestion extends UnaryMathsQuestion {

    public FibonacciQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        int n = random + 4;
        if (n > 20 && n % 10 == 1) {
            return "what is the " + n + "st number in the Fibonacci sequence";
        } else if (n > 20 && n % 10 == 2) {
            return "what is the " + n + "nd number in the Fibonacci sequence";
        }
        return "what is the " + n + "th number in the Fibonacci sequence";
    }

    @Override
    public String correctAnswer() {
        int n = random + 4;
        return String.valueOf(fib(n));
    }

    @Override
    public int getPoints() {
        return 50;
    }

    private long fib(int n) {
        if (n <= 1) {
            return n;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }
}
