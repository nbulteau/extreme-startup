package fr.nbu.extremestartup.server.domain.question.selectfromlist;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public abstract class SelectFromListOfNumbersQuestion extends Question {

    protected final List<Integer> numbers = new ArrayList<>();

    public SelectFromListOfNumbersQuestion(Player player) {
        super(player);

        Set<Integer> candidateNumbers = new HashSet<>();
        int size = random(1, 3);
        do {
            int random = random(1, 100);
            candidateNumbers.add(random);
        } while (candidateNumbers.size() < size);
        do {
            candidateNumbers.add(candidateNumber());
        } while (candidateNumbers.size() < size * 2);
        numbers.addAll(candidateNumbers);
    }

    @Override
    public String correctAnswer() {
        // ArrayList to conserve the order
        List<Integer> correctAnswers = new ArrayList<>();
        for (Integer element : numbers) {
            if (shouldBeSelected(element)) {
                correctAnswers.add(element);
            }
        }

        return StringUtils.collectionToCommaDelimitedString(correctAnswers);
    }

    protected abstract boolean shouldBeSelected(int x);

    protected abstract Integer candidateNumber();

}
