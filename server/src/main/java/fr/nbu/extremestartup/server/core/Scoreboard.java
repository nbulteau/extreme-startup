package fr.nbu.extremestartup.server.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import fr.nbu.extremestartup.server.core.logger.EventWriter;
import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class Scoreboard {

    private final boolean lenient;

    /**
     * The LinkedHashMap is important to the solution as it provides predictable
     * iteration order => Needed to get the leaderboard
     */
    private final Map<UUID, Integer> scores = new LinkedHashMap<>();

    private final Map<UUID, Integer> correcTally = new HashMap<>();
    private final Map<UUID, Integer> incorrectTally = new HashMap<>();
    private final Map<UUID, Integer> requestCounts = new HashMap<>();

    public Scoreboard(boolean lenient) {
        super();
        this.lenient = lenient;
    }

    public void incrementScoreForPlayer(Player player, Question question) {
        int increment = score(question, leaderboardPosition(player));

        Integer score = scores.get(player.getUuid());
        scores.put(player.getUuid(), score + increment);
        EventWriter.logIncrementScore(player, increment, score);

        if (increment > 0) {
            score = correcTally.get(player.getUuid());
            correcTally.put(player.getUuid(), score + 1);
        } else {
            score = incorrectTally.get(player.getUuid());
            incorrectTally.put(player.getUuid(), score + 1);
        }

        player.logResult(question.asText() + " => " + question.getAnswer(),
                question.getResult().name(), increment);
    }

    public void recordRequestForlayer(Player player) {
        Integer score = requestCounts.get(player.getUuid());
        requestCounts.put(player.getUuid(), score + 1);
    }

    public void newPlayer(Player player) {
        scores.put(player.getUuid(), 0);
        correcTally.put(player.getUuid(), 0);
        incorrectTally.put(player.getUuid(), 0);
        requestCounts.put(player.getUuid(), 0);
    }

    public void deletePlayer(Player player) {
        scores.remove(player.getUuid());
    }

    public Integer currentScore(Player player) {
        return scores.get(player.getUuid());
    }

    public Integer currentTotalCorrect(Player player) {
        return correcTally.get(player.getUuid());
    }

    public Integer currentTotalNotCorrect(Player player) {
        return incorrectTally.get(player.getUuid());
    }

    public Integer totalRequestsFor(Player player) {
        return requestCounts.get(player.getUuid());
    }

    /**
     * Sort a the Scores Map
     * 
     * @param map
     * @return
     */
    private Map<UUID, Integer> sortByValue(Map<UUID, Integer> map) {
        List<Map.Entry<UUID, Integer>> list = new LinkedList<>(map.entrySet());

        Collections.sort(list,
                (o1, o2) -> o1.getValue().compareTo(o2.getValue()));

        Map<UUID, Integer> result = new LinkedHashMap<>(list.size());
        for (Map.Entry<UUID, Integer> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public Map<UUID, Integer> getLeaderboard() {
        // sort scores
        return sortByValue(scores);
    }

    public int leaderboardPosition(Player player) {
        int index = 0;
        Map<UUID, Integer> sortedScores = getLeaderboard();
        Iterator<Entry<UUID, Integer>> iterator = sortedScores.entrySet()
                .iterator();
        boolean found = false;
        while (iterator.hasNext() && !found) {
            Entry<UUID, Integer> entry = iterator.next();
            index++;
            if (player.getUuid() == entry.getKey()) {
                found = true;
            }
        }
        return index;
    }

    public int score(Question question, int leaderboardPosition) {
        switch (question.getResult()) {
        case CORRECT:
            return question.getPoints();
        case WRONG:
            return lenient ? allowPasses(question, leaderboardPosition)
                    : penalty(leaderboardPosition);
        default:
            return -20;
        }
    }

    private int allowPasses(Question question, int leaderboardPosition) {
        return "".equals(question.getAnswer()) ? 0
                : penalty(leaderboardPosition);
    }

    private int penalty(int leaderboardPosition) {
        return -1 * Question.BASE_POINT / leaderboardPosition;
    }
}
