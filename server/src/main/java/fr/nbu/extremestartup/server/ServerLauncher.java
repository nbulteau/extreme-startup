package fr.nbu.extremestartup.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.nbu.extremestartup.server.core.ExtremeStartupQuestionFactory;
import fr.nbu.extremestartup.server.core.ExtremeStartupServer;
import fr.nbu.extremestartup.server.core.GameState;
import fr.nbu.extremestartup.server.core.QuestionFactory;
import fr.nbu.extremestartup.server.core.Scoreboard;
import fr.nbu.extremestartup.server.core.WarmupQuestionFactory;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class ServerLauncher {

	private static final Logger LOGGER = LoggerFactory
			.getLogger("eXtremeStartup");

	public static void main(String[] args) {
		LOGGER.info("eXtreme Startup ServerLauncher started.");

		SpringApplication.run(ServerLauncher.class, args);
	}

	@Bean
	public QuestionFactory questionFactory() {
		String warmupProperty = System.getProperty("warmup");
		if (warmupProperty != null && "true".equals(warmupProperty)) {
			return new WarmupQuestionFactory();
		} else {
			return new ExtremeStartupQuestionFactory();
		}
	}

	@Bean
	public GameState gameState() {
		return new GameState();
	}

	@Bean
	public Scoreboard scoreboard() {
		boolean lenient = false;
		String lenientProperty = System.getProperty("lenient");
		if (lenientProperty != null && "true".equals(lenientProperty)) {
			lenient = true;
		}
		return new Scoreboard(lenient);
	}

	@Bean
	ExtremeStartupServer extremeStartupServer() {
		return new ExtremeStartupServer(gameState(), scoreboard(),
				questionFactory());
	}

}
