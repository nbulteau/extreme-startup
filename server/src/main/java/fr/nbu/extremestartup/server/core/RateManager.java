package fr.nbu.extremestartup.server.core;

import fr.nbu.extremestartup.server.domain.question.Question;

/**
 * 
 * @author Nicolas
 *
 */
public class RateManager {

    private static final float MIN_REQUEST_INTERVAL_SECS = 2;
    private static final float MAX_REQUEST_INTERVAL_SECS = 20;
    private static final float REQUEST_DELTA_SECS = 0.1f;

    private static final int SLASHDOT_THRESHOLD_SCORE = 2000;

    private static final float SLASH_DOT_PROBABILITY_PERCENT = 0.1f;

    protected float delay = 5;

    public void waitForNextRequest(final Question question)
            throws InterruptedException {
        Thread.sleep(delayBeforeNextRequest(question));
    }

    public long delayBeforeNextRequest(final Question question) {
        if (question.wasAnsweredCorrectly()) {
            if (delay > MIN_REQUEST_INTERVAL_SECS) {
                delay = delay - REQUEST_DELTA_SECS;
            }
        } else if (question.wasAnsweredWrongly()) {
            if (delay < MAX_REQUEST_INTERVAL_SECS) {
                delay = delay + REQUEST_DELTA_SECS;
            }
        } else
        // error response
        if (delay < 10) {
            delay = 10f;
        }
        return (long) delay * 1000;
    }

    public RateManager updateAlgorithmBasedOnScore(int score) {
        if (score > SLASHDOT_THRESHOLD_SCORE
                && (Math.random() * 1000) < (10 * SLASH_DOT_PROBABILITY_PERCENT)) {
            return new SlashdotRateManager();
        }
        return this;
    }
}
