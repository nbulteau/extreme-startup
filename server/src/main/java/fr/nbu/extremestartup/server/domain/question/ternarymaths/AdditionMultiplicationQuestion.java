package fr.nbu.extremestartup.server.domain.question.ternarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class AdditionMultiplicationQuestion extends TernaryMathsQuestion {

    public AdditionMultiplicationQuestion(Player player) {
        super(player);
    }

    @Override
    public String asText() {
        return "what is " + n1 + " plus " + n2 + " multiplied by " + n3;
    }

    @Override
    public String correctAnswer() {
        return String.valueOf(n1 + n2 * n3);
    }

    @Override
    public int getPoints() {
        return 60;
    }

}
