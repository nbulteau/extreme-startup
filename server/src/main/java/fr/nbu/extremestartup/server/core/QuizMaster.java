package fr.nbu.extremestartup.server.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.nbu.extremestartup.server.core.logger.EventWriter;
import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

/**
 * Class QuizMaster
 * @author Nicolas
 *
 */
public class QuizMaster extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger("eXtremeStartup");

    private final Player player;

    private final Scoreboard scoreboard;

    private final QuestionFactory questionFactory;

    private final GameState gameState;

    private RateManager rateController = new RateManager();

    /**
     * 
     * @param player
     * @param scoreboard
     * @param questionFactory
     * @param gameState
     */
    public QuizMaster(final Player player, final Scoreboard scoreboard,
    		final QuestionFactory questionFactory, final GameState gameState) {
        super(player.getName());
        this.player = player;
        this.scoreboard = scoreboard;
        this.questionFactory = questionFactory;
        this.gameState = gameState;
    }

    @Override
    public void run() {

        while (!isInterrupted()) {
            try {
                if (gameState.isRunning()) {
                    Question question = questionFactory.nextQuestion(player);

                    long start = System.currentTimeMillis();
                    question.ask(10);
                    question.setDuration(System.currentTimeMillis() - start);

                    EventWriter.logQuestion(player, question);

                    scoreboard.recordRequestForlayer(player);
                    scoreboard.incrementScoreForPlayer(player, question);

                    rateController.waitForNextRequest(question);
                    rateController = rateController
                            .updateAlgorithmBasedOnScore(scoreboard
                                    .currentScore(player));
                } else {
                    Thread.sleep(2 * 1000);
                }

            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
                LOGGER.warn("Interrupted Exception : {}", ie.getMessage());
            } catch (Exception e) {
                LOGGER.warn("Exception : {}", e.getMessage());
            }
        }
    }
}
