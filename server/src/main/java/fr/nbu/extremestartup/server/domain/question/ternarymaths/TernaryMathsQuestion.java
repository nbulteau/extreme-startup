package fr.nbu.extremestartup.server.domain.question.ternarymaths;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public abstract class TernaryMathsQuestion extends Question {

    protected final int n1;

    protected final int n2;

    protected final int n3;

    public TernaryMathsQuestion(Player player) {
        super(player);
        this.n1 = random(1, 10);
        this.n2 = random(1, 10);
        this.n3 = random(1, 10);
    }

}
