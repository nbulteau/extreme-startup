package fr.nbu.extremestartup.server.domain.question.unarymaths;

import fr.nbu.extremestartup.server.domain.Player;

public class FahrenheitToCelsiusQuestion extends UnaryMathsQuestion {

	public FahrenheitToCelsiusQuestion(Player player) {
		super(player);
	}

	@Override
	public String asText() {
		return "how much is " + random + " Fahrenheit in Celsius (2 digits after decimal point)";
	}

	@Override
	public String correctAnswer() {
		return String.format("%.2f", (float) (random - 32) * 5 / 9);
	}
}
