package fr.nbu.extremestartup.server.domain.question.miscellaneous;

import java.util.HashMap;
import java.util.Map;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

public class GeneralKnowledgeQuestion extends Question {

    private static final Map<String, String> QUESTION_BANK = new HashMap<>();

    private final String currentQuestion;

    // setup QUESTION_BANK
    static {
        QUESTION_BANK.put("what does 'RTFM' stand for",
                "Read The Fucking Manual");
        QUESTION_BANK.put("who counted to infinity twice", "Chuck Norris");
        QUESTION_BANK
                .put("what is the answer to life, the universe and everything",
                        "42");
        QUESTION_BANK.put("who said 'Luke, I am your father'", "Darth Vader");
        QUESTION_BANK.put("who is the Prime Minister of Great Britain",
                "David Cameron");
        QUESTION_BANK.put("what colour is a banana", "yellow");
        QUESTION_BANK.put("who played James Bond in the film Dr No",
                "Sean Connery");
        QUESTION_BANK.put("what is the capital city of Botswana", "Gaborone");
        QUESTION_BANK
                .put("What is that which in the morning goeth upon four feet; up on two feet in the afternoon; and in the Evening upon three?",
                        "Man");
    }

    public GeneralKnowledgeQuestion(Player player) {
        super(player);
        int random = random(0, QUESTION_BANK.size() - 1);
        currentQuestion = (String) QUESTION_BANK.keySet().toArray()[random];
    }

    @Override
    public String asText() {
        return currentQuestion;
    }

    @Override
    public String correctAnswer() {
        return QUESTION_BANK.get(currentQuestion);
    }

}
