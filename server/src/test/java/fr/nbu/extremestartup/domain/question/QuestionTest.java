package fr.nbu.extremestartup.domain.question;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;
import fr.nbu.extremestartup.server.domain.question.binarymaths.AdditionQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.BlackjackQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.HexadecimalQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.MinusQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.MultQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.PlusQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.PowerQuestion;
import fr.nbu.extremestartup.server.domain.question.binarymaths.SubtractionQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.GeneralArithmeticQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.GeneralKnowledgeQuestion;
import fr.nbu.extremestartup.server.domain.question.miscellaneous.WeekdayQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.MaximumQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.PrimesQuestion;
import fr.nbu.extremestartup.server.domain.question.selectfromlist.SquareCubeQuestion;
import fr.nbu.extremestartup.server.domain.question.string.AlphagramQuestion;
import fr.nbu.extremestartup.server.domain.question.string.MaxblockQuestion;
import fr.nbu.extremestartup.server.domain.question.string.PalindromeQuestion;
import fr.nbu.extremestartup.server.domain.question.string.ParenBitQuestion;
import fr.nbu.extremestartup.server.domain.question.string.SHA1Question;
import fr.nbu.extremestartup.server.domain.question.string.ScrabbleQuestion;
import fr.nbu.extremestartup.server.domain.question.ternarymaths.AdditionAdditionQuestion;
import fr.nbu.extremestartup.server.domain.question.ternarymaths.AdditionMultiplicationQuestion;
import fr.nbu.extremestartup.server.domain.question.ternarymaths.MultiplicationAdditionQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FahrenheitToCelsiusQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FeetToMetersQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.FibonacciQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.PiQuestion;
import fr.nbu.extremestartup.server.domain.question.unarymaths.ReverseNumberQuestion;

@RunWith(MockitoJUnitRunner.class)
public class QuestionTest {

	@Mock
	private Player player;

	@Test
	public void testMaxblockQuestion() {
		// Given
		Question question = new MaxblockQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testParenBitQuestion() {
		// Given
		Question question = new ParenBitQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testBlackjackQuestion() {
		// Given
		Question question = new BlackjackQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testWeekdayQuestion() {
		// Given
		Question question = new WeekdayQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testAlphagramQuestion() {
		// Given
		Question question = new AlphagramQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testPiQuestion() {
		// Given
		Question question = new PiQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testReverseNumberQuestion() {
		// Given
		Question question = new ReverseNumberQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testPalindromeQuestion() {
		// Given
		Question question = new PalindromeQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testFahrenheitToCelsiusQuestion() {
		// Given
		Question question = new FahrenheitToCelsiusQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testScrabbleQuestion() {
		// Given
		Question question = new ScrabbleQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testSHA1Question() {
		// Given
		Question question = new SHA1Question(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testFeetToMetersQuestion() {
		// Given
		Question question = new FeetToMetersQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testHexadecimalQuestion() {
		// Given
		Question question = new HexadecimalQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testAdditionAdditionQuestion() {
		// Given
		Question question = new AdditionAdditionQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testAdditionMultiplicationQuestion() {
		// Given
		Question question = new AdditionMultiplicationQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testAdditionQuestion() {
		// Given
		Question question = new AdditionQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testFibonacciQuestion() {
		// Given
		Question question = new FibonacciQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testGeneralArithmeticQuestion() {
		// Given
		Question question = new GeneralArithmeticQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testGeneralKnowledgeQuestion() {
		// Given
		Question question = new GeneralKnowledgeQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testMaximumQuestion() {
		// Given
		Question question = new MaximumQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testMinusQuestion() {
		// Given
		Question question = new MinusQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testMultiplicationAdditionQuestion() {
		// Given
		Question question = new MultiplicationAdditionQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testMultiplicationQuestion() {
		// Given
		Question question = new MultiplicationAdditionQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testMultQuestion() {
		// Given
		Question question = new MultQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testPlusQuestion() {
		// Given
		Question question = new PlusQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testPowerQuestion() {
		// Given
		Question question = new PowerQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testPrimesQuestion() {
		// Given
		Question question = new PrimesQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testSquareCubeQuestion() {
		// Given
		Question question = new SquareCubeQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	@Test
	public void testSubtractionQuestion() {
		// Given
		Question question = new SubtractionQuestion(player);

		// When

		// Then
		displayQuestion(question);
	}

	private void displayQuestion(Question question) {
		String text = question.asText();
		String answer = question.correctAnswer();

		System.out.println("------");
		System.out.println(question.getClass().getSimpleName());
		System.out.println(" * Question : " + text);
		System.out.println(" * Answer : " + answer);
	}

}
