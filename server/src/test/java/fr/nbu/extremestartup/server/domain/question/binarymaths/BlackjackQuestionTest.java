package fr.nbu.extremestartup.server.domain.question.binarymaths;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

@RunWith(MockitoJUnitRunner.class)
public class BlackjackQuestionTest {

	@Mock
	private Player player;

	@Test
	public void testBlackjackQuestion1() {
		// Given
		Question question = new BlackjackQuestion(player, 19, 20);

		// When

		// Then
		Assert.assertEquals("20", question.correctAnswer());
	}

	@Test
	public void testBlackjackQuestion2() {
		// Given
		Question question = new BlackjackQuestion(player, 19, 22);

		// When

		// Then
		Assert.assertEquals("19", question.correctAnswer());
	}

	@Test
	public void testBlackjackQuestion3() {
		// Given
		Question question = new BlackjackQuestion(player, 22, 19);

		// When

		// Then
		Assert.assertEquals("19", question.correctAnswer());
	}

	@Test
	public void testBlackjackQuestion4() {
		// Given
		Question question = new BlackjackQuestion(player, 23, 23);

		// When

		// Then
		Assert.assertEquals("0", question.correctAnswer());
	}

	@Test
	public void testBlackjackQuestion5() {
		// Given
		Question question = new BlackjackQuestion(player, 21, 21);

		// When

		// Then
		Assert.assertEquals("21", question.correctAnswer());
	}

}
