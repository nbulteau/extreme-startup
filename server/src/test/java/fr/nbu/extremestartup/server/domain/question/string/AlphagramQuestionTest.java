package fr.nbu.extremestartup.server.domain.question.string;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

@RunWith(MockitoJUnitRunner.class)
public class AlphagramQuestionTest {

	@Mock
	private Player player;

	@Test
	public void testAlphagramQuestion1() {
		// Given
		Question question = new AlphagramQuestion(player, "mongodb");

		// When

		// Then
		Assert.assertEquals("bdgmnoo", question.correctAnswer());

	}

	@Test
	public void testAlphagramQuestion2() {
		// Given
		Question question = new AlphagramQuestion(player, "cloud");

		// When

		// Then
		Assert.assertEquals("cdlou", question.correctAnswer());

	}

	@Test
	public void testAlphagramQuestion3() {
		// Given
		Question question = new AlphagramQuestion(player, "bigdata");

		// When

		// Then
		Assert.assertEquals("aabdgit", question.correctAnswer());

	}

	@Test
	public void testAlphagramQuestion4() {
		// Given
		Question question = new AlphagramQuestion(player, "yottabytes");

		// When

		// Then
		Assert.assertEquals("abeostttyy", question.correctAnswer());

	}

	@Test
	public void testAlphagramQuestion5() {
		// Given
		Question question = new AlphagramQuestion(player, "bazinga");

		// When

		// Then
		Assert.assertEquals("aabginz", question.correctAnswer());

	}

	@Test
	public void testAlphagramQuestion6() {
		// Given
		Question question = new AlphagramQuestion(player, "virtualisation");

		// When

		// Then
		Assert.assertEquals("aaiiilnorsttuv", question.correctAnswer());

	}

}
