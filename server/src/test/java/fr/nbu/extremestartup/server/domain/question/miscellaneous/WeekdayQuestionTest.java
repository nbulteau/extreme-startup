package fr.nbu.extremestartup.server.domain.question.miscellaneous;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

@RunWith(MockitoJUnitRunner.class)
public class WeekdayQuestionTest {

	@Mock
	private Player player;

	private final DateFormat sdf = new SimpleDateFormat("dd MM yyyy");

	@Test
	public void testWeekdayQuestion1() throws ParseException {
		// Given
		Date date = sdf.parse("29 03 2014");
		Question question = new WeekdayQuestion(player, date);

		// When

		// Then
		Assert.assertEquals("samedi", question.correctAnswer());
	}

}
