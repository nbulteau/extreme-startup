package fr.nbu.extremestartup.server.domain.question.string;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

@RunWith(MockitoJUnitRunner.class)
public class MaxblockQuestionTest {

	@Mock
	private Player player;

	@Test
	public void testMaxblockQuestion1() {
		// Given
		Question question = new MaxblockQuestion(player, "hoopla");

		// When

		// Then
		Assert.assertEquals("2", question.correctAnswer());

	}

	@Test
	public void testMaxblockQuestion2() {
		// Given
		Question question = new MaxblockQuestion(player, "aabbbcccBBBBddd");

		// When

		// Then
		Assert.assertEquals("4", question.correctAnswer());

	}

	@Test
	public void testMaxblockQuestion3() {
		// Given
		Question question = new MaxblockQuestion(player, "kayak");

		// When

		// Then
		Assert.assertEquals("1", question.correctAnswer());

	}

	@Test
	public void testMaxblockQuestion4() {
		// Given
		Question question = new MaxblockQuestion(player, "desserts");

		// When

		// Then
		Assert.assertEquals("2", question.correctAnswer());

	}

	@Test
	public void testMaxblockQuestion5() {
		// Given
		Question question = new MaxblockQuestion(player, "XX112222BBBbbXX112222");

		// When

		// Then
		Assert.assertEquals("4", question.correctAnswer());

	}

}
