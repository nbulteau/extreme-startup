package fr.nbu.extremestartup.server.domain.question.string;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.nbu.extremestartup.server.domain.Player;
import fr.nbu.extremestartup.server.domain.question.Question;

@RunWith(MockitoJUnitRunner.class)
public class ParenBitQuestionTest {

	@Mock
	private Player player;

	@Test
	public void testParenBitQuestion1() {
		// Given
		Question question = new ParenBitQuestion(player, "this (is a) test");

		// When

		// Then
		Assert.assertEquals("(is a)", question.correctAnswer());

	}

	@Test
	public void testParenBitQuestion2() {
		// Given
		Question question = new ParenBitQuestion(player, "zyxw(abcd)54321");

		// When

		// Then
		Assert.assertEquals("(abcd)", question.correctAnswer());

	}

	@Test
	public void testParenBitQuestion3() {
		// Given
		Question question = new ParenBitQuestion(player, "123(riddle)");

		// When

		// Then
		Assert.assertEquals("(riddle)", question.correctAnswer());

	}

	@Test
	public void testParenBitQuestion4() {
		// Given
		Question question = new ParenBitQuestion(player, "(kayak)");

		// When

		// Then
		Assert.assertEquals("(kayak)", question.correctAnswer());

	}

	@Test
	public void testParenBitQuestion5() {
		// Given
		Question question = new ParenBitQuestion(player, "(bazinga)desserts");

		// When

		// Then
		Assert.assertEquals("(bazinga)", question.correctAnswer());

	}

	   @Test
	    public void testParenBitQuestion6() {
	        // Given
	        Question question = new ParenBitQuestion(player, "12345(monster)abc");

	        // When

	        // Then
	        Assert.assertEquals("(monster)", question.correctAnswer());

	    }

}
