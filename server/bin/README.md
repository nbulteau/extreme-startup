Welcome
=======
This is the java version of Robert Chatley and Matt Wynne "Extreme Startup" Ruby version . 
This software supports a workshop where teams can compete to build a software product that satisfies market demand.

Keywords : Java 7, Spring Boot, thymeleaf, Jetty

Getting started
---------------
* Install a JDK > 1.7
* Install maven > 3.x
* Start the game server
````
 mvn package && java -jar target/gs-spring-boot-0.1.0.jar
````

Launch a lenient Extreme Startup Server : -Dlenient=true
````
 mvn package && java -jar target/gs-spring-boot-0.1.0.jar -Dlenient=true
````

Launch a warmup Extreme Startup Server : -Dwarmup=true
````
 mvn package && java -jar target/gs-spring-boot-0.1.0.jar -Dwarmup=true
````
http://localhost:8080
http://localhost:8080/player
http://localhost:8080/graph
http://localhost:8080/controlpanel

Notes for facilitators
----------------------
* Run the server on your machine. It's a Jetty app that by default runs on port 8080.
* Everyone needs a computer connected to the same network, so that they can communicate. Check that everyone can see the leaderboard page served by the webapp running on your machine. Depending on the situation, we have used a local/ad-hoc network and that is ok for the game.

* Warmup round: run the web server with the `WARMUP` environment variable set:

If you consider hosting an Extreme Startup session yourself, this checklist might help:

* Make sure that all participants have a skeleton project before joining or your warmup round might take longer than expected.
* Tell the participants to check whether their servers can be reached from the network. We experienced all kind of weird firewall issues during our session especially on some Windows laptops, so if there are problems, make sure you can ping clients from the server and vice versa.
* Participants need to be able to write an application that responds to HTTP GET requests. You should also consider telling them that they should be prepared to parse strings.
* Play through all rounds before hosting the session. Be prepared to deal with bugs in the game server.


<table>
	<thead>
		<tr>
			<th rowspan="2">Question</th>
			<th rowspan="2">Point</th>
			<th colspan="8">Rounds</th>
		</tr>
		<tr>
			<th>1</th>
			<th>2</th>
			<th>3</th>
			<th>4</th>
			<th>5</th>
			<th>6</th>
			<th>7</th>
			<th>8</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>AdditionQuestion</td>
			<td>10</td>
			<td>x</td>
			<td></td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>MaximumQuestion</td>
			<td>40</td>
			<td>x</td>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>MultiplicationQuestion</td>
			<td>30</td>
			<td/>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>SquareCubeQuestion</td>
			<td>60</td>
			<td/>
			<td>x</td>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>GeneralKnowledgeQuestion</td>
			<td>10</td>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>PrimesQuestion</td>
			<td>60</td>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>SubtractionQuestion</td>
			<td>10</td>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>FibonacciQuestion</td>
			<td>50</td>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>PowerQuestion</td>
			<td>20</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td/>
			<td/>
		</tr>
		<tr>
			<td>AdditionAdditionQuestion</td>
			<td>30</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td>x</td>
			<td/>
		</tr>
		<tr>
			<td>AdditionMultiplicationQuestion</td>
			<td>60</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td/>
		</tr>
		<tr>
			<td>MultiplicationAdditionQuestion</td>
			<td>50</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
			<td>x</td>
		</tr>
		<tr>
			<td>AnagramQuestion</td>
			<td>10</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
		</tr>
		<tr>
			<td>ScrabbleQuestion</td>
			<td>10</td>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td/>
			<td>x</td>
			<td>x</td>
		</tr>
	</tbody>
	<tfooter>
		<tr>
			<td/>
			<td/>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
			<td>7</td>
			<td>8</td>
		</tr>
	</tfooter>
</table>

Finally, I'd like to thank Matt and Robert for the awesome idea and for providing the Ruby version game server! And Stéphane Moreau for the FeetToMetersQuestion, PiQuestion and HexadecimalQuestion (https://github.com/xebia-france/xke-extreme-startup)

