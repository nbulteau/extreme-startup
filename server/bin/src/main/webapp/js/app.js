var chart;
var poolDelay = 2500;
var colors = [ 'red', 'green', 'blue', 'cyan', 'yellow', 'magenta', 'sienna', 'pink', 'blueviolet', 'Orange', 'lime', 'white', 'olive', 'tomato' ];

var url = '/scores';
var xhr = new XMLHttpRequest();

function requestData() {
	$.ajax({
		url : url,
		success : function(data) {

			var i, lng = data.leaderBoardEntries.length;
			var o = {}, point = {}, shift;

			for (i = 0; i < lng; i += 1) {

				if (typeof chart.series[i] !== 'object') {

					o.name = data.leaderBoardEntries[i].playerName;
					o.id = data.leaderBoardEntries[i].playerId;
					o.data = [];
					o.color = colors[i];
					chart.addSeries(o);
				}
				shift = chart.series[i].data.length > 20;
				point.x = new Date().getTime() + 7200000;
				point.y = data.leaderBoardEntries[i].score;
				point.color = colors[i];
					

				chart.series[i].addPoint(point, true, shift);

			}

			setTimeout(requestData, poolDelay);
		},
		cache : false
	});
}
$(document).ready(function() {
	chart = new Highcharts.Chart({
		chart : {
			renderTo : 'container',
			defaultSeriesType : 'spline',
			events : {
				load : requestData
			}
		},
		title : {
			text : 'Live Board'
		},
		xAxis : {
			type : 'datetime',
			tickPixelInterval : 150,
			maxZoom : 20 * 1000
		},
		yAxis : {
			minPadding : 0.2,
			maxPadding : 0.2,
			title : {
				text : 'Value',
				margin : 80
			}
		},
		series : []
	});
});