package fr.nbu.extremestartup.scoreviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScoreEvent {

	private static final SimpleDateFormat ISO_DATETIME_TIME_ZONE_FORMAT_WITH_MILLIS = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSX");

	private long timestamp;
	private UUID uuid;
	private String name;
	private int increment;
	private int newScore;

	public ScoreEvent() {
		super();
	}

	public long getTimestamp() {
		return timestamp;
	}

	@JsonProperty("@timestamp")
	public void setTimestamp(String timestamp) throws ParseException {
		Date date = ISO_DATETIME_TIME_ZONE_FORMAT_WITH_MILLIS.parse(timestamp);
		this.timestamp = date.getTime();
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIncrement() {
		return increment;
	}

	public void setIncrement(int increment) {
		this.increment = increment;
	}

	public int getNewScore() {
		return newScore;
	}

	public void setNewScore(int newScore) {
		this.newScore = newScore;
	}

}
