package fr.nbu.extremestartup.scoreviewer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ScoreEventReader {
	private static final Logger LOGGER = Logger.getLogger(ScoreEventReader.class.getName());

	public static List<ScoreEvent> readScoreEvents(InputStream in) {
		final List<ScoreEvent> scoreEvents = new LinkedList<>();
		final ObjectMapper objectMapper = new ObjectMapper();

		String currentLine = null;
		try (BufferedReader out = new BufferedReader(new InputStreamReader(in))) {
			while ((currentLine = out.readLine()) != null) {
				if (currentLine.contains("\"message\":\"score\"")) {
					ScoreEvent scoreEvent = objectMapper.readValue(currentLine, ScoreEvent.class);
					scoreEvents.add(scoreEvent);
				}
			}
		} catch (IOException ioe) {
			LOGGER.info(ioe.getMessage());
		}
		return scoreEvents;
	}

}