package fr.nbu.extremestartup.scoreviewer;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class ScoreChart extends Application {
    private static final String LOG_FILE = "events.log";
	private final static String TITLE = "eXtreme Startup At Rennes - 2014-04-03";

	@Override
	public void start(Stage stage) {
	    
		stage.setTitle(TITLE);
		final NumberAxis xAxis = new NumberAxis();
		xAxis.setLabel("Number of requests");
		final NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Score");
		final LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);

		lineChart.setTitle(TITLE);

		Map<String, XYChart.Series<Number, Number>> series = new HashMap<>();
		int i = 0;

		for (ScoreEvent scoreEvent : ScoreEventReader.readScoreEvents(ScoreChart.class.getResourceAsStream(LOG_FILE))) {
			if (!series.containsKey(scoreEvent.getName())) {
				XYChart.Series<Number, Number> newSerie = new XYChart.Series<>();
				series.put(scoreEvent.getName(), newSerie);
				newSerie.setName(scoreEvent.getName());
			}
			series.get(scoreEvent.getName()).getData().add(new XYChart.Data<Number, Number>(i++, scoreEvent.getNewScore()));
		}

		Scene scene = new Scene(lineChart, 1340, 800);

		for (Entry<String, Series<Number, Number>> playerSeriesEntry : series.entrySet()) {
			lineChart.getData().add(playerSeriesEntry.getValue());
		}

		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
